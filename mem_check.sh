#!/bin/bash

# Define the device to check
DEVICE="/dev/mmcblk0"

# Define the log file
LOG_FILE="/var/log/badblocks_check.log"

# Get the current timestamp
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')

# Run badblocks check and append the output to the log file with a timestamp
{
    echo "[$TIMESTAMP] Starting badblocks check on $DEVICE"
    sudo badblocks -v $DEVICE 10000 0
    echo "[$TIMESTAMP] Completed badblocks check on $DEVICE"
} >> $LOG_FILE 2>&1

echo "Badblocks check completed. Log saved to $LOG_FILE."
