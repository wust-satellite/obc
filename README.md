# OBC

## Project Overview
![Raspberry Pi testing platform](images/pi_photo.png)
![Yocto Project](images/yocto_logo.png)
![BitBake Programming Language](images/bitbake_logo.png)

The Raspberry Pi and the Yocto Project for its operating system offer a robust and flexible platform for various space missions. Developers can leverage a familiar and flexible development environment, enabling efficient and reliable mission-specific customization and operations.

## System Functions
- Use a lite OS and custom scripts to minimize power consumption.
- Collect data sent from the LoRa Radio Module.
- Send information to the ground station via the LoRa Radio Module.
- Use a watchdog for redundant/failover mechanisms.
- Perform cyclical memory tests to monitor memory corruption.
- Create log files of satellite events.

## Testing Platform
For testing the scripts and new functions, Raspberry Pi OS Lite was used as a compact and easy operating system with all the Raspberry Pi hardware tools.

## Software: Yocto Project
The Yocto Project is a popular choice for building custom Linux distributions for embedded systems, including CubeSats. It provides a flexible and highly configurable environment.

## Further Applications
The powerful hardware and operating system of the micro-computer can launch Python scripts, analyze data onboard, and operate sensors.

### Example Use Cases: 
- **Earth Observation**: Equip the CubeSat with cameras and sensors to monitor environmental changes, weather patterns, or natural disasters.
- **Technology Demonstration**: Test new space technologies such as advanced communication systems, propulsion methods, or materials in the space environment.
- **Scientific Research**: Conduct experiments in microgravity or study cosmic phenomena using specialized instruments.

## Sources
- [Raspberry Pi Documentation](https://www.raspberrypi.com/documentation/)
- [Yocto Project Documentation](https://docs.yoctoproject.org/)
- [Yocto Part 4: Building a Basic Image for Raspberry Pi](https://kickstartembedded.com/2021/12/22/yocto-part-4-building-a-basic-image-for-raspberry-pi/)
