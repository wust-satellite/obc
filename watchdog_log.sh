#!/bin/bash

# Define the log file
LOG_FILE="/var/log/watchdog_reboots.log"

# Get the current timestamp
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')

# Check system logs for watchdog reboots
# You may need to adjust the grep pattern based on your system logs
WATCHDOG_LOGS=$(sudo journalctl -b -1 | grep -i "watchdog" | grep -i "reboot")

# Append the logs to the log file with a timestamp
if [[ ! -z "$WATCHDOG_LOGS" ]]; then
    {
        echo "[$TIMESTAMP] Watchdog reboot detected:"
        echo "$WATCHDOG_LOGS"
    } >> $LOG_FILE 2>&1
else
    echo "[$TIMESTAMP] No watchdog reboot detected" >> $LOG_FILE 2>&1
fi

echo "Watchdog reboot check completed. Log saved to $LOG_FILE."
